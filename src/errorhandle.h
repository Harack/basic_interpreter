#pragma once

/*��������ö��*/
typedef enum Error_type
{
    malloc_error,
    stack_pop_error,
    stack_top_error,
    operand_stack_not_empty,
    token_type_error,
    var_not_assign,
    sscanf_error,
    undefined_oper,
    expression_error,
    open_file_error,
    line_num_error,
    code_too_long,
    parse_src_error,
    illeage_var_name,
    too_much_for,
    too_much_if,
    connot_find_next,
    connot_find_for,
    connot_find_endif,
    connot_find_if,
    connot_find_target_line,
    illeage_target,
    too_much_while,
    connot_find_wend,
    connot_find_while,
    empty_expression,
    undefined_func
} errortype;

void errorhandle(int, int);