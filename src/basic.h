#pragma once
#define MEMORY_SIZE 26
#define CODE_SIZE 10000
#define STRING_SIZE 128

/* 变量类型枚举：未定义型、字符型、双精度型*/
typedef enum Var_type
{
    vartype_null,
    vartype_string,
    vartype_double
} vartype;

typedef char string[STRING_SIZE]; /* 字符型定义 */

/*变量定义：字符类型、字符值（数字或字符串）*/
typedef struct Var
{
    vartype type;
    union
    {
        double num;
        string str;
    };
} VAR;

/*二十六个预定义变量*/
VAR memory[MEMORY_SIZE];
