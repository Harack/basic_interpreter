#include "expression.h"
#include "errorhandle.h"
#include "basic.h"
#include "load.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

TOKEN last;              /*上一个Token*/
const char *iter = NULL; /*传入的字符串*/

/*所有操作符：*/
const OPERATOR operators[] = {
    {2, 17, 1, left2right, oper_left},
    {2, 17, 17, left2right, oper_right},
    {2, 12, 12, left2right, oper_add},
    {2, 12, 12, left2right, oper_sub},
    {2, 13, 13, left2right, oper_mult},
    {2, 13, 13, left2right, oper_div},
    {2, 13, 13, left2right, oper_mod},
    {1, 16, 15, right2left, oper_positive},
    {1, 16, 15, right2left, oper_negative},
    //
    {2, 10, 10, left2right, oper_less},
    {2, 10, 10, left2right, oper_greater},
    {2, 9, 9, left2right, oper_equal},
    {2, 9, 9, left2right, oper_ne},
    {2, 10, 10, left2right, oper_le},
    {2, 10, 10, left2right, oper_ge},
    //
    {2, 5, 5, left2right, oper_and},
    {2, 4, 4, left2right, oper_or},
    {1, 15, 15, right2left, oper_not},
    //
    {2, 2, 2, right2left, oper_assign},
    {2, 0, 0, right2left, oper_bottom}};

/*初始化Token栈*/
PLIST stack_init()
{
    PLIST p = (PLIST)malloc(sizeof(TOKEN_LIST));
    if (p == NULL)
    {
        errorhandle(code[now_line].num, malloc_error);
    }
    else
    {
        p->next = NULL;
    }
    return p;
}

/*Token栈入栈操作*/
void stack_push(PLIST st, const TOKEN x)
{
    PLIST p = (PLIST)malloc(sizeof(TOKEN_LIST));
    if (p == NULL)
    {
        errorhandle(code[now_line].num, malloc_error);
    }
    else
    {
        p->token = x;
        p->next = st->next;
        st->next = p;
    }
}

/*Token栈出栈操作*/
void stack_pop(PLIST st)
{
    PLIST p = st->next;
    if (p == NULL)
    {
        errorhandle(code[now_line].num, stack_pop_error);
    }
    else
    {
        st->next = p->next;
        free(p);
    }
}

/*获得Token栈栈顶数据*/
TOKEN stack_top(const PLIST st)
{
    TOKEN temp;
    if (st == NULL || st->next == NULL)
    {
        errorhandle(code[now_line].num, stack_top_error);
        exit(-1);
    }
    else
    {
        temp = (st->next)->token;
        return temp;
    }
}

/*释放Token栈内存*/
void stack_delete(PLIST st)
{
    while (st->next != NULL)
    {
        stack_pop(st);
    }
    free(st);
}

/*词法分析：获得下一个Token*/
TOKEN get_token()
{
    TOKEN token;
    token.type = token_unknow;
    token.var.type = vartype_null;
    string s;
    int i;
    if (iter == NULL)//空字符串检查
    {
        return token;
    }
    while (*iter && isspace(*iter))//跳过字符串中的空格
    {
        iter++;
    }
    if (*iter == '\0')//空字符串检查
    {
        return token;
    }
    if (*iter == '"')//字符型变量值
    {
        token.type = token_operand;
        token.var.type = vartype_string;
        iter++;
        for (i = 0; *iter && *iter != '"'; i++)
        {
            token.var.str[i] = *iter;
            iter++;
        }
        token.var.str[i] = '\0';
        iter++;
    }
    else if (isalpha(*iter))//非字符型变量或操作符
    {
        token.type = token_operator;
        for (i = 0; isalnum(*iter); i++)//小写转换为大写
        {
            s[i] = toupper(*iter);
            iter++;
        }
        s[i] = '\0';
        if (!strcmp(s, "AND"))//操作符AND
        {
            token.oper = operators[oper_and];
        }
        else if (!strcmp(s, "OR"))//操作符OR
        {
            token.oper = operators[oper_or];
        }
        else if (!strcmp(s, "NOT"))//操作符NOT
        {
            token.oper = operators[oper_not];
        }
        else if (i == 1)
        {
            token.type = token_operand;
            token.var = memory[s[0] - 'A'];
            if (token.var.type == vartype_null)//使用未赋初值的变量则报错
            {
                errorhandle(code[now_line].num, var_not_assign);
            }
        }
        else
        {
            errorhandle(code[now_line].num, token_type_error);//输入为未定义Token类型则报错
        }
    }
    else if (isdigit(*iter) || *iter == '.')//浮点型变量
    {
        token.type = token_operand;
        token.var.type = vartype_double;
        for (i = 0; isdigit(*iter) || *iter == '.'; i++)
        {
            s[i] = *iter;
            iter++;
        }
        s[i] = '\0';
        if (sscanf(s, "%lf", &token.var.num) != 1)
        {
            errorhandle(code[now_line].num, sscanf_error);
        }
    }
    else
    {
        token.type = token_operator;
        switch (*iter)//根据输入判断操作符并赋相应值
        {
        case '(':
            token.oper = operators[oper_left];
            break;
        case ')':
            token.oper = operators[oper_right];
            break;
        case '+':
            if ((last.type == token_operator && last.oper.oper != oper_right) || last.type == token_unknow)
            {
                token.oper = operators[oper_positive];
            }
            else
            {
                token.oper = operators[oper_add];
            }
            break;
        case '-':
            if ((last.type == token_operator && last.oper.oper != oper_right) || last.type == token_unknow)
            {
                token.oper = operators[oper_negative];
            }
            else
            {
                token.oper = operators[oper_sub];
            }
            break;
        case '*':
            token.oper = operators[oper_mult];
            break;
        case '/':
            token.oper = operators[oper_div];
            break;
        case '%':
            token.oper = operators[oper_mod];
            break;
        case '<':
            if (*(iter + 1) == '=')
            {
                token.oper = operators[oper_le];
                iter++;
            }
            else
            {
                token.oper = operators[oper_less];
            }
            break;
        case '>':
            if (*(iter + 1) == '=')
            {
                token.oper = operators[oper_ge];
                iter++;
            }
            else
            {
                token.oper = operators[oper_greater];
            }
            break;
        case '!':
            if (*(iter + 1) == '=')
            {
                token.oper = operators[oper_ne];
                iter++;
            }
            else
            {
                token.oper = operators[oper_not];
            }
            break;
        case '=':
            token.oper = operators[oper_equal];
            break;
        default:
            errorhandle(code[now_line].num, undefined_oper);
            break;
        }
        iter++;
    }
    last = token;
    return token;
}

/*调试：输出Token*/
void test_token(TOKEN x)
{
    if (x.type == token_operand)
    {
        if (x.var.type == vartype_double)
        {
            printf("test is double: %lf\n", x.var.num);
        }
        else if (x.var.type == vartype_string)
        {
            printf("test is string: %s\n", x.var.str);
        }
    }
    else
    {
        printf("test is operator: %d\n", x.oper.oper);
    }
}

/*表达式值计算：返回一个变量*/
VAR eval(const char *eval)
{
    PLIST operator_stack = stack_init();//初始化操作数栈
    PLIST operand_stack = stack_init();//初始化操作符栈

    TOKEN token, temp, a, b, c;

    iter = eval;
    last.type = token_unknow;
    token.type = token_operator;
    token.oper = operators[oper_bottom];
    stack_push(operator_stack, token);//为方便编写代码，将栈底操作符加入操作符栈
    while (*iter)//中缀表达式计算表达式值
    {
        token = get_token();//获取下一个Token
        // test_token(token);//Token调试
        // printf("get token\n");
        if (token.type == token_operand)
        {
            stack_push(operand_stack, token);//若为操作数，则压入操作数栈
            continue;
        }
        else if (token.type == token_operator)//若为操作符
        {
            temp = stack_top(operator_stack);//与操作符栈顶操作符比较
            int judge = 1;
            while (judge && temp.oper.oper != oper_bottom && (token.oper.icp <= temp.oper.isp || token.oper.oper == oper_right))//优先级更低，则计算
            {
                stack_pop(operator_stack);//操作符栈顶操作符出栈
                switch (temp.oper.oper)//根据操作符进行相关运算
                {
                case oper_add:
                    a = stack_top(operand_stack);
                    stack_pop(operand_stack);
                    b = stack_top(operand_stack);
                    stack_pop(operand_stack);
                    if (a.var.type == vartype_string && b.var.type == vartype_string)
                    {
                        c.type = token_operand;
                        c.var.type = vartype_string;
                        strcat(b.var.str, a.var.str);
                        strcpy(c.var.str, b.var.str);
                        stack_push(operand_stack, c);
                    }
                    else if (a.var.type == vartype_double && b.var.type == vartype_double)
                    {
                        c.var.num = a.var.num + b.var.num;
                        c.type = token_operand;
                        c.var.type = vartype_double;
                        stack_push(operand_stack, c);
                    }
                    else
                    {
                        errorhandle(code[now_line].num, expression_error);
                    }
                    break;
                case oper_sub:
                    a = stack_top(operand_stack);
                    stack_pop(operand_stack);
                    b = stack_top(operand_stack);
                    stack_pop(operand_stack);
                    c.var.num = b.var.num - a.var.num;
                    if (a.var.type != vartype_double || b.var.type != vartype_double)
                    {
                        errorhandle(code[now_line].num, expression_error);
                        exit(-1);
                    }
                    c.type = token_operand;
                    c.var.type = vartype_double;
                    stack_push(operand_stack, c);
                    break;
                case oper_mult:
                    a = stack_top(operand_stack);
                    stack_pop(operand_stack);
                    b = stack_top(operand_stack);
                    stack_pop(operand_stack);
                    if (a.var.type != vartype_double || b.var.type != vartype_double)
                    {
                        errorhandle(code[now_line].num, expression_error);
                        exit(-1);
                    }
                    c.var.num = b.var.num * a.var.num;
                    c.type = token_operand;
                    c.var.type = vartype_double;
                    stack_push(operand_stack, c);
                    break;
                case oper_div:
                    a = stack_top(operand_stack);
                    stack_pop(operand_stack);
                    b = stack_top(operand_stack);
                    stack_pop(operand_stack);
                    if (a.var.type != vartype_double || b.var.type != vartype_double)
                    {
                        errorhandle(code[now_line].num, expression_error);
                        exit(-1);
                    }
                    c.var.num = b.var.num / a.var.num;
                    c.type = token_operand;
                    c.var.type = vartype_double;
                    stack_push(operand_stack, c);
                    break;
                case oper_mod:
                    a = stack_top(operand_stack);
                    stack_pop(operand_stack);
                    b = stack_top(operand_stack);
                    stack_pop(operand_stack);
                    if (a.var.type != vartype_double || b.var.type != vartype_double)
                    {
                        errorhandle(code[now_line].num, expression_error);
                        exit(-1);
                    }
                    c.var.num = (int)b.var.num % (int)a.var.num;
                    c.type = token_operand;
                    c.var.type = vartype_double;
                    stack_push(operand_stack, c);
                    break;
                case oper_less:
                    a = stack_top(operand_stack);
                    stack_pop(operand_stack);
                    b = stack_top(operand_stack);
                    stack_pop(operand_stack);
                    if (a.var.type != vartype_double || b.var.type != vartype_double)
                    {
                        errorhandle(code[now_line].num, expression_error);
                        exit(-1);
                    }
                    c.var.num = b.var.num < a.var.num;
                    c.type = token_operand;
                    c.var.type = vartype_double;
                    stack_push(operand_stack, c);
                    break;
                case oper_greater:
                    a = stack_top(operand_stack);
                    stack_pop(operand_stack);
                    b = stack_top(operand_stack);
                    stack_pop(operand_stack);
                    if (a.var.type != vartype_double || b.var.type != vartype_double)
                    {
                        errorhandle(code[now_line].num, expression_error);
                        exit(-1);
                    }
                    c.var.num = b.var.num > a.var.num;
                    c.type = token_operand;
                    c.var.type = vartype_double;
                    stack_push(operand_stack, c);
                    break;
                case oper_equal:
                    a = stack_top(operand_stack);
                    stack_pop(operand_stack);
                    b = stack_top(operand_stack);
                    stack_pop(operand_stack);
                    c.var.num = b.var.num == a.var.num;
                    c.type = token_operand;
                    c.var.type = vartype_double;
                    stack_push(operand_stack, c);
                    break;
                case oper_ne:
                    a = stack_top(operand_stack);
                    stack_pop(operand_stack);
                    b = stack_top(operand_stack);
                    stack_pop(operand_stack);
                    c.var.num = b.var.num != a.var.num;
                    c.type = token_operand;
                    c.var.type = vartype_double;
                    stack_push(operand_stack, c);
                    break;
                case oper_le:
                    a = stack_top(operand_stack);
                    stack_pop(operand_stack);
                    b = stack_top(operand_stack);
                    stack_pop(operand_stack);
                    if (a.var.type != vartype_double || b.var.type != vartype_double)
                    {
                        errorhandle(code[now_line].num, expression_error);
                        exit(-1);
                    }
                    c.var.num = b.var.num <= a.var.num;
                    c.type = token_operand;
                    c.var.type = vartype_double;
                    stack_push(operand_stack, c);
                    break;
                case oper_ge:
                    a = stack_top(operand_stack);
                    stack_pop(operand_stack);
                    b = stack_top(operand_stack);
                    stack_pop(operand_stack);
                    if (a.var.type != vartype_double || b.var.type != vartype_double)
                    {
                        errorhandle(code[now_line].num, expression_error);
                        exit(-1);
                    }
                    c.var.num = b.var.num >= a.var.num;
                    c.type = token_operand;
                    c.var.type = vartype_double;
                    stack_push(operand_stack, c);
                    break;
                case oper_and:
                    a = stack_top(operand_stack);
                    stack_pop(operand_stack);
                    b = stack_top(operand_stack);
                    stack_pop(operand_stack);
                    if (a.var.type != vartype_double || b.var.type != vartype_double)
                    {
                        errorhandle(code[now_line].num, expression_error);
                        exit(-1);
                    }
                    c.var.num = b.var.num && a.var.num;
                    c.type = token_operand;
                    c.var.type = vartype_double;
                    stack_push(operand_stack, c);
                    break;
                case oper_or:
                    a = stack_top(operand_stack);
                    stack_pop(operand_stack);
                    b = stack_top(operand_stack);
                    stack_pop(operand_stack);
                    if (a.var.type != vartype_double || b.var.type != vartype_double)
                    {
                        errorhandle(code[now_line].num, expression_error);
                        exit(-1);
                    }
                    c.var.num = b.var.num || a.var.num;
                    c.type = token_operand;
                    c.var.type = vartype_double;
                    stack_push(operand_stack, c);
                    break;
                case oper_not:
                    b = stack_top(operand_stack);
                    stack_pop(operand_stack);
                    if (b.var.type != vartype_double)
                    {
                        errorhandle(code[now_line].num, expression_error);
                        exit(-1);
                    }
                    c.var.num = !b.var.num;
                    c.type = token_operand;
                    c.var.type = vartype_double;
                    stack_push(operand_stack, c);
                    break;
                case oper_positive:
                    b = stack_top(operand_stack);
                    stack_pop(operand_stack);
                    if (b.var.type != vartype_double)
                    {
                        errorhandle(code[now_line].num, expression_error);
                        exit(-1);
                    }
                    c.var.num = b.var.num;
                    c.type = token_operand;
                    c.var.type = vartype_double;
                    stack_push(operand_stack, c);
                    break;
                case oper_negative:
                    b = stack_top(operand_stack);
                    stack_pop(operand_stack);
                    if (b.var.type != vartype_double)
                    {
                        errorhandle(code[now_line].num, expression_error);
                        exit(-1);
                    }
                    c.var.num = -b.var.num;
                    c.type = token_operand;
                    c.var.type = vartype_double;
                    stack_push(operand_stack, c);
                    break;
                case oper_left:
                    if (*iter == '\0')
                    {
                        judge = 0;
                        break;
                    }
                    token = get_token();
                    break;
                default:
                    break;
                }
                temp = stack_top(operator_stack);
            }
            stack_push(operator_stack, token);
        }
        else if (token.type == token_unknow)
        {
            break;
        }
        else
        {
            errorhandle(code[now_line].num, expression_error);
            exit(-1);
        }
    }
    temp = stack_top(operator_stack);//计算操作符栈中剩余的操作符
    while (temp.oper.oper != oper_bottom)//取出剩下的操作符
    {
        temp = stack_top(operator_stack);
        stack_pop(operator_stack);
        switch (temp.oper.oper)//根据操作符进行相关运算
        {
        case oper_add:
            a = stack_top(operand_stack);
            stack_pop(operand_stack);
            b = stack_top(operand_stack);
            stack_pop(operand_stack);
            if (a.var.type == vartype_string && b.var.type == vartype_string)
            {
                c.type = token_operand;
                c.var.type = vartype_string;
                strcat(b.var.str, a.var.str);
                strcpy(c.var.str, b.var.str);
                stack_push(operand_stack, c);
            }
            else if (a.var.type == vartype_double && b.var.type == vartype_double)
            {
                c.var.num = a.var.num + b.var.num;
                c.type = token_operand;
                c.var.type = vartype_double;
                stack_push(operand_stack, c);
            }
            else
            {
                errorhandle(code[now_line].num, expression_error);
            }
            break;
        case oper_sub:
            a = stack_top(operand_stack);
            stack_pop(operand_stack);
            b = stack_top(operand_stack);
            stack_pop(operand_stack);
            if (a.var.type != vartype_double || b.var.type != vartype_double)
            {
                errorhandle(code[now_line].num, expression_error);
                exit(-1);
            }
            c.var.num = b.var.num - a.var.num;
            c.type = token_operand;
            c.var.type = vartype_double;
            stack_push(operand_stack, c);
            break;
        case oper_mult:
            a = stack_top(operand_stack);
            stack_pop(operand_stack);
            b = stack_top(operand_stack);
            stack_pop(operand_stack);
            if (a.var.type != vartype_double || b.var.type != vartype_double)
            {
                errorhandle(code[now_line].num, expression_error);
                exit(-1);
            }
            c.var.num = b.var.num * a.var.num;
            c.type = token_operand;
            c.var.type = vartype_double;
            stack_push(operand_stack, c);
            break;
        case oper_div:
            a = stack_top(operand_stack);
            stack_pop(operand_stack);
            b = stack_top(operand_stack);
            stack_pop(operand_stack);
            if (a.var.type != vartype_double || b.var.type != vartype_double)
            {
                errorhandle(code[now_line].num, expression_error);
                exit(-1);
            }
            c.var.num = b.var.num / a.var.num;
            c.type = token_operand;
            c.var.type = vartype_double;
            stack_push(operand_stack, c);
            break;
        case oper_mod:
            a = stack_top(operand_stack);
            stack_pop(operand_stack);
            b = stack_top(operand_stack);
            stack_pop(operand_stack);
            if (a.var.type != vartype_double || b.var.type != vartype_double)
            {
                errorhandle(code[now_line].num, expression_error);
                exit(-1);
            }
            c.var.num = (int)b.var.num % (int)a.var.num;
            c.type = token_operand;
            c.var.type = vartype_double;
            stack_push(operand_stack, c);
            break;
        case oper_less:
            a = stack_top(operand_stack);
            stack_pop(operand_stack);
            b = stack_top(operand_stack);
            stack_pop(operand_stack);
            if (a.var.type != vartype_double || b.var.type != vartype_double)
            {
                errorhandle(code[now_line].num, expression_error);
                exit(-1);
            }
            c.var.num = b.var.num < a.var.num;
            c.type = token_operand;
            c.var.type = vartype_double;
            stack_push(operand_stack, c);
            break;
        case oper_greater:
            a = stack_top(operand_stack);
            stack_pop(operand_stack);
            b = stack_top(operand_stack);
            stack_pop(operand_stack);
            if (a.var.type != vartype_double || b.var.type != vartype_double)
            {
                errorhandle(code[now_line].num, expression_error);
                exit(-1);
            }
            c.var.num = b.var.num > a.var.num;
            c.type = token_operand;
            c.var.type = vartype_double;
            stack_push(operand_stack, c);
            break;
        case oper_equal:
            a = stack_top(operand_stack);
            stack_pop(operand_stack);
            b = stack_top(operand_stack);
            stack_pop(operand_stack);
            c.var.num = b.var.num == a.var.num;
            c.type = token_operand;
            c.var.type = vartype_double;
            stack_push(operand_stack, c);
            break;
        case oper_ne:
            a = stack_top(operand_stack);
            stack_pop(operand_stack);
            b = stack_top(operand_stack);
            stack_pop(operand_stack);
            c.var.num = b.var.num != a.var.num;
            c.type = token_operand;
            c.var.type = vartype_double;
            stack_push(operand_stack, c);
            break;
        case oper_le:
            a = stack_top(operand_stack);
            stack_pop(operand_stack);
            b = stack_top(operand_stack);
            stack_pop(operand_stack);
            if (a.var.type != vartype_double || b.var.type != vartype_double)
            {
                errorhandle(code[now_line].num, expression_error);
                exit(-1);
            }
            c.var.num = b.var.num <= a.var.num;
            c.type = token_operand;
            c.var.type = vartype_double;
            stack_push(operand_stack, c);
            break;
        case oper_ge:
            a = stack_top(operand_stack);
            stack_pop(operand_stack);
            b = stack_top(operand_stack);
            stack_pop(operand_stack);
            if (a.var.type != vartype_double || b.var.type != vartype_double)
            {
                errorhandle(code[now_line].num, expression_error);
                exit(-1);
            }
            c.var.num = b.var.num >= a.var.num;
            c.type = token_operand;
            c.var.type = vartype_double;
            stack_push(operand_stack, c);
            break;
        case oper_and:
            a = stack_top(operand_stack);
            stack_pop(operand_stack);
            b = stack_top(operand_stack);
            stack_pop(operand_stack);
            if (a.var.type != vartype_double || b.var.type != vartype_double)
            {
                errorhandle(code[now_line].num, expression_error);
                exit(-1);
            }
            c.var.num = b.var.num && a.var.num;
            c.type = token_operand;
            c.var.type = vartype_double;
            stack_push(operand_stack, c);
            break;
        case oper_or:
            a = stack_top(operand_stack);
            stack_pop(operand_stack);
            b = stack_top(operand_stack);
            stack_pop(operand_stack);
            if (a.var.type != vartype_double || b.var.type != vartype_double)
            {
                errorhandle(code[now_line].num, expression_error);
                exit(-1);
            }
            c.var.num = b.var.num || a.var.num;
            c.type = token_operand;
            c.var.type = vartype_double;
            stack_push(operand_stack, c);
            break;
        case oper_not:
            b = stack_top(operand_stack);
            stack_pop(operand_stack);
            if (b.var.type != vartype_double)
            {
                errorhandle(code[now_line].num, expression_error);
                exit(-1);
            }
            c.var.num = !b.var.num;
            c.type = token_operand;
            c.var.type = vartype_double;
            stack_push(operand_stack, c);
            break;
        case oper_positive:
            b = stack_top(operand_stack);
            stack_pop(operand_stack);
            if (b.var.type != vartype_double)
            {
                errorhandle(code[now_line].num, expression_error);
                exit(-1);
            }
            c.var.num = b.var.num;
            c.type = token_operand;
            c.var.type = vartype_double;
            stack_push(operand_stack, c);
            break;
        case oper_negative:
            b = stack_top(operand_stack);
            stack_pop(operand_stack);
            if (b.var.type != vartype_double)
            {
                errorhandle(code[now_line].num, expression_error);
                exit(-1);
            }
            c.var.num = -b.var.num;
            c.type = token_operand;
            c.var.type = vartype_double;
            stack_push(operand_stack, c);
            break;
        default:
            break;
        }
        temp = stack_top(operator_stack);
    }
    if (operand_stack->next == NULL)//若操作数栈为空，则表达式无返回值，传入表达式为空，报错
    {
        stack_delete(operand_stack);
        stack_delete(operator_stack);
        errorhandle(code[now_line].num, empty_expression);
        exit(-1);
    }
    c = stack_top(operand_stack);//取出表达式计算结果
    stack_pop(operand_stack);
    if (operand_stack->next == NULL)
    {
        stack_delete(operand_stack);
        stack_delete(operator_stack);//释放相应空间
        return c.var;//返回表达式值
    }
    else//若操作符栈有剩余，则表达式非法，报错
    {
        stack_delete(operand_stack);
        stack_delete(operator_stack);
        errorhandle(code[now_line].num, operand_stack_not_empty);
        return c.var;
    }
}
