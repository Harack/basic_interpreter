#include "basic.h"
#include "expression.h"
#include "errorhandle.h"
#include "load.h"
#include "parsing.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//#define DEBUG //输入为Debug文件

#ifdef DEBUG

int main()
{
    loadCode("debug.bas");
    return exec();
}

#else

/*程序主函数*/
int main(int argc, char *argv[])
{
    loadCode(argv[1]);//从main函数参数中读取文件名称
    int res = exec();//运行程序，获得返回值
    if (res == 0)//返回值为0表示程序运行正常结束
    {
        printf("PROGRAM END SUCCESS\n");
        char ch = getchar();
        ch = getchar();//暂停控制台，不让其自动关闭（等价于system("pause");）
        return 0;
    }
    else//程序运行异常，报错RE
    {
        printf("RUN ERROR\n");
        char ch = getchar();
        ch = getchar();//暂停控制台，不让其自动关闭（等价于system("pause");）
        return -1;
    }
}

#endif