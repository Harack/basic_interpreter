#include "basic.h"
#include "load.h"
#include "parsing.h"
#include "expression.h"
#include "errorhandle.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#pragma warning(disable : 4996)

int top_for = -1;
int top_if = -1;
int top_while = -1;

/*函数指针数组：便于调用*/
void (*key_func[])(const char *) = {
    exec_input,
    exec_print,
    exec_for,
    exec_next,
    exec_while,
    exec_wend,
    exec_if,
    exec_else,
    exec_endif,
    exec_goto,
    exec_assign};

/*获得当前行的关键字*/
keywords get_key(const char *src)
{
    if (!strncmp(src, "INPUT ", 6))
    {
        return key_input;
    }
    else if (!strncmp(src, "PRINT ", 6))
    {
        return key_print;
    }
    else if (!strncmp(src, "FOR ", 4))
    {
        return key_for;
    }
    else if (!strncmp(src, "NEXT", 4))
    {
        return key_next;
    }
    else if (!strncmp(src, "WHILE ", 6))
    {
        return key_while;
    }
    else if (!strncmp(src, "WEND", 4))
    {
        return key_wend;
    }
    else if (!strncmp(src, "IF ", 3))
    {
        return key_if;
    }
    else if (!strncmp(src, "ELSE", 4))
    {
        return key_else;
    }
    else if (!strncmp(src, "END IF", 6))
    {
        return key_endif;
    }
    else if (!strncmp(src, "GOTO ", 5))
    {
        return key_goto;
    }
    else if (!strncmp(src, "LET ", 4))
    {
        return key_let;
    }
    else if (strlen(src) == 0)
    {
        return -1;
    }
    else
    {
        return -2;
    }
}

/*执行INPUT操作*/
void exec_input(const char *src)
{
    if (src == NULL)
    {
        errorhandle(code[now_line].num, parse_src_error);
    }
    const char *expr = src + 5;
    while (*expr)
    {
        while (*expr && isspace(*expr))
        {
            expr++;
        }
        if (!isalpha(*expr) || isalnum(*(expr + 1)))//INPUT后跟随非法变量，报错
        {
            errorhandle(code[now_line].num, illeage_var_name);
        }
        else
        {
            int target = toupper(*expr) - 'A';
            if (target < 0 || target >= 26)
            {
                errorhandle(code[now_line].num, illeage_var_name);//INPUT后跟随非法变量，报错
            }
            else
            {
                if (!scanf("%lf", &memory[target].num))
                {
                    memory[target].type = vartype_string;
                    if (!scanf("%s", memory[target].str))
                    {
                        errorhandle(code[now_line].num, sscanf_error);
                    }
                }
                else
                {
                    memory[target].type = vartype_double;
                }
            }
        }
        expr++;
        while (*expr && isspace(*expr))
        {
            expr++;
        }
        if (*expr && *expr != '\n' && *expr != '\0' && *expr != ',')//INPUT语句结尾错误
        {
            errorhandle(code[now_line].num, expression_error);
        }
        else if (*expr)
        {
            expr++;
        }
    }
}

/*执行PRINT操作*/
void exec_print(const char *src)
{
    if (src == NULL)
    {
        errorhandle(code[now_line].num, parse_src_error);
    }
    const char *expr = src + 5;
    VAR ans = eval(expr);
    if (ans.type == vartype_double)
    {
        printf("%g\n", ans.num);
    }
    else if (ans.type == vartype_string)
    {
        printf("%s\n", ans.str);
    }
    else
    {
        errorhandle(code[now_line].num, var_not_assign);//使用未定义变量
        exit(-1);
    }
}

/*执行FOR循环操作*/
void exec_for(const char *src)
{
    if (src == NULL)
    {
        errorhandle(code[now_line].num, parse_src_error);
    }
    const char *str1 = src + 4;
    int top = top_for + 1;
    if (top > MEMORY_SIZE)
    {
        errorhandle(code[now_line].num, too_much_for);//FOR循环嵌套过多，报错
    }
    while (*str1 && isspace(*str1))
    {
        str1++;
    }
    if (isalpha(*str1) && !isalnum(*(str1 + 1)))
    {
        for_stack[top].start_var = toupper(*str1) - 'A';
        for_stack[top].line = now_line;
    }
    else
    {
        errorhandle(code[now_line].num, expression_error);//FOR循环内部表达式非法，报错
    }
    str1++;
    while (*str1 && isspace(*str1))
    {
        str1++;
    }
    if (*str1 != '=')
    {
        errorhandle(code[now_line].num, expression_error);//FOR循环内部表达式非法，报错
    }
    str1++;
    const char *str2 = strstr(str1, "TO");
    if (str2 == NULL)
    {
        errorhandle(code[now_line].num, expression_error);//FOR循环内部表达式非法，报错
    }
    string expr1, expr2;
    int i;
    for (i = 0; str1 != str2 && *str1; i++)
    {
        expr1[i] = *str1;
        str1++;
    }
    expr1[i] = '\0';
    memory[for_stack[top].start_var] = eval(expr1);//初始化循环变量
    str1 += 3;
    while (*str1 && isspace(*str1))
    {
        str1++;
    }
    const char *str3 = strstr(str1, "STEP");
    if (str3 == NULL)
    {
        for_stack[top].step = 1;
        for_stack[top].target = eval(str1).num;
    }
    else//计算步长
    {
        str3 += 5;
        while (*str3 && isspace(*str3))
        {
            str3++;
        }
        if (*str3 != '=')
        {
            errorhandle(code[now_line].num, expression_error);
        }
        str3++;
        for_stack[top].step = eval(str3).num;
        str3 = strstr(str1, "STEP");
        for (i = 0; str1 != str3 && *str1; i++)
        {
            expr2[i] = *str1;
            str1++;
        }
        expr2[i] = '\0';
        for_stack[top].target = eval(expr2).num;
    }
    int target_line = 0;
    if (for_stack[top].step == 0)
    {
        errorhandle(code[now_line].num, expression_error);//循环步长为0，非法
    }
    else//寻找NEXT
    {
        top_for++;
        int for_count = 0;
        target_line = now_line + 1;
        while (target_line <= code_size)
        {
            if (!strncmp(code[target_line].code, "FOR ", 4))
            {
                for_count++;
            }
            if (!strncmp(code[target_line].code, "NEXT ", 4))
            {
                for_count--;
            }
            if (for_count == -1)
            {
                break;
            }
            target_line++;
        }
        if (target_line > code_size)
        {
            errorhandle(code[now_line].num, connot_find_next);//没有与FOR配对的NEXT，报错
        }
    }
    if ((memory[for_stack[top_for].start_var].num > for_stack[top_for].target && for_stack[top_for].step > 0) ||
        (memory[for_stack[top_for].start_var].num < for_stack[top_for].target && for_stack[top_for].step < 0))
    {
        now_line = target_line - 1;//不会进入循环，跳出
    }
}

/*执行NEXT操作*/
void exec_next(const char *src)
{
    if (src == NULL)
    {
        errorhandle(code[now_line].num, parse_src_error);
    }
    if (top_for == -1)
    {
        errorhandle(code[now_line].num, connot_find_for);//没有对应的FOR，报错
    }
    memory[for_stack[top_for].start_var].num += for_stack[top_for].step;
    if ((memory[for_stack[top_for].start_var].num > for_stack[top_for].target && for_stack[top_for].step > 0) ||
        (memory[for_stack[top_for].start_var].num < for_stack[top_for].target && for_stack[top_for].step < 0))
    {
        top_for--;//循环结束，跳出
    }
    else
    {
        now_line = for_stack[top_for].line;//循环跳转
    }
}

/*执行WHILE操作*/
void exec_while(const char *src)
{
    if (src == NULL)
    {
        errorhandle(code[now_line].num, parse_src_error);
    }
    const char *expr = src + 6;
    int top = top_while + 1;
    if (top >= MEMORY_SIZE)
    {
        errorhandle(code[now_line].num, too_much_while);//WHILE层数超过限制
    }
    int target_line = now_line + 1;
    int while_count = 0;
    while (target_line <= code_size)//寻找WEND
    {
        if (!strncmp(code[target_line].code, "WHILE", 5))
        {
            while_count++;
        }
        if (!strncmp(code[target_line].code, "WEND", 4))
        {
            while_count--;
        }
        if (while_count == -1)
        {
            break;
        }
        target_line++;
    }
    if (target_line > code_size)
    {
        errorhandle(code[now_line].num, connot_find_wend);//没有对应的WEND，报错
    }
    VAR ans = eval(expr);//计算循环便量
    if (while_stack[top].judge != 1)
    {
        if (ans.num == 0)//不进入循环
        {
            now_line = target_line;
        }
        else//循环
        {
            top_while++;
            while_stack[top_while].judge = 1;
            while_stack[top_while].line = now_line;
        }
    }
    else
    {
        if (ans.num == 0)//循环结束
        {
            top_while--;
            now_line = target_line;
        }
    }
}

/*执行WEND操作*/
void exec_wend(const char *src)
{
    if (src == NULL)
    {
        errorhandle(code[now_line].num, parse_src_error);
    }
    if (top_while == -1)
    {
        errorhandle(code[now_line].num, connot_find_while);//未找到对应的WHILE
    }
    now_line = while_stack[top_while].line - 1;//执行循环
}

/*执行IF操作*/
void exec_if(const char *src)
{
    if (src == NULL)
    {
        errorhandle(code[now_line].num, parse_src_error);
    }
    int top = top_if + 1;
    if (top_if >= MEMORY_SIZE)
    {
        errorhandle(code[now_line].num, too_much_if);//IF嵌套过多，报错
    }
    const char *str1 = src + 3;
    const char *str2 = strstr(src, "THEN");
    string expr;
    if (str2 == NULL)
    {
        errorhandle(code[now_line].num, expression_error);//IF后没有THEN，报错
    }
    else
    {
        int i;
        for (i = 0; *str1 && str1 != str2; i++)
        {
            expr[i] = *str1;
            str1++;
        }
        expr[i] = '\0';
    }
    top_if++;
    VAR ans = eval(expr);
    if ((ans.type == vartype_double && ans.num != 0) || (ans.type == vartype_string && ans.str != ""))
    {
        if_stack[top] = 1;
    }
    else
    {
        if_stack[top] = -1;
        int target_line = now_line;
        int if_count = 0;
        while (target_line <= code_size)//寻找对应END IF、ELSE
        {
            target_line++;
            if (!strncmp(code[target_line].code, "IF ", 3))
            {
                if_count++;
            }
            if (!strncmp(code[target_line].code, "END IF", 6))
            {
                if_count--;
            }
            if (!strncmp(code[target_line].code, "ELSE", 4) && if_count == 0)
            {
                break;
            }
            if (if_count == -1)
            {
                break;
            }
        }
        if (target_line > code_size)
        {
            errorhandle(code[now_line].num, connot_find_endif);//没有对应END IF，报错
        }
        now_line = target_line;
    }
}

/*执行ELSE操作*/
void exec_else(const char *src)
{
    if (src == NULL)
    {
        errorhandle(code[now_line].num, parse_src_error);
    }
    if (top_if == -1)
    {
        errorhandle(code[now_line].num, connot_find_if);//未找到对应的IF，报错
    }
    if (if_stack[top_if] == 1)
    {
        int target_line = now_line;
        int if_count = 0;
        while (target_line <= code_size)//寻找ELSE对应的IF和END IF
        {
            target_line++;
            if (!strncmp(code[target_line].code, "IF ", 3))
            {
                if_count++;
            }
            if (!strncmp(code[target_line].code, "END IF", 6))
            {
                if_count--;
            }
            if (if_count == -1)
            {
                break;
            }
        }
        if (target_line > code_size)
        {
            errorhandle(code[now_line].num, connot_find_endif);//未找到对应的END IF，报错
        }
        now_line = target_line;
    }
}

/*执行ENDIF操作*/
void exec_endif(const char *src)
{
    if (src == NULL)
    {
        errorhandle(code[now_line].num, parse_src_error);
    }
    if (top_if == -1)
    {
        errorhandle(code[now_line].num, connot_find_if);//未找到对应的IF，报错
    }
    else
    {
        top_if--;
    }
}

/*执行GOTO操作*/
void exec_goto(const char *src)
{
    const char *expr = src + 5;
    VAR ans = eval(expr);
    int target_line = now_line;
    if (ans.num < code[now_line].num)//向上跳转
    {
        while (target_line >= 0)
        {
            if (code[target_line].num == ans.num)
            {
                now_line = target_line - 1;
                break;
            }
            target_line--;
        }
        if (target_line < 0)
        {
            errorhandle(code[now_line].num, connot_find_target_line);//未找到跳转目标行号，报错
        }
    }
    else//向下跳转
    {
        while (target_line <= code_size)
        {
            if (code[target_line].num == ans.num)
            {
                now_line = target_line - 1;
                break;
            }
            target_line++;
        }
        if (target_line > code_size)
        {
            errorhandle(code[now_line].num, connot_find_target_line);//未找到跳转目标行号，报错
        }
    }
    if (all_code[target_line].for_count != 0 || all_code[target_line].if_count != 0)
    {
        errorhandle(code[now_line].num, illeage_target);//跳转到IF中或FOR中非法
    }
    top_for = all_code[target_line].for_count;
    top_if = all_code[target_line].if_count;
    top_while = all_code[target_line].while_count;//更新相应的跳转语句层数
}

/*执行ASSIGN操作*/
void exec_assign(const char *src)
{
    if (src == NULL)
    {
        errorhandle(code[now_line].num, parse_src_error);
    }
    const char *expr = src + 3;
    int target;
    while (*expr && isspace(*expr))
    {
        expr++;
    }
    if (!isalpha(*expr) || isalnum(*(expr + 1)))
    {
        errorhandle(code[now_line].num, illeage_var_name);//非法变量名，报错
    }
    else
    {
        target = toupper(*expr) - 'A';
        expr++;
        while (*expr && isspace(*expr))
        {
            expr++;
        }
        if (*expr != '=')
        {
            errorhandle(code[now_line].num, expression_error);//未寻找到等号，报错
        }
        else
        {
            expr++;
            memory[target] = eval(expr);
        }
    }
}

/*初始化每一行的跳转指令数量*/
void init_goto()
{
    int temp_line = 0;
    int if_count = 0;
    int for_count = 0;
    int while_count = 0;
    while (temp_line <= code_size)//初始化每一行的跳转层数
    {
        if (get_key(code[temp_line].code) == key_if)
        {
            if_count++;
        }
        else if (get_key(code[temp_line].code) == key_for)
        {
            for_count++;
        }
        else if (get_key(code[temp_line].code) == key_while)
        {
            while_count++;
        }
        else if (get_key(code[temp_line].code) == key_endif)
        {
            if_count--;
        }
        else if (get_key(code[temp_line].code) == key_next)
        {
            for_count--;
        }
        else if (get_key(code[temp_line].code) == key_wend)
        {
            while_count--;
        }
        all_code[temp_line].for_count = for_count;
        all_code[temp_line].if_count = if_count;
        all_code[temp_line].while_count = while_count;
        temp_line++;
    }
}

/*开始解释输入程序*/
int exec()
{
    init_goto();
    while (now_line <= code_size)
    {
        int func_key = get_key(code[now_line].code);//获取当前语句关键字
        if (func_key == -1)//程序结束
            return 0;
        else if (func_key == -2)
        {
            errorhandle(code[now_line].num, undefined_func);//未定义关键字，报错
        }
        (*key_func[func_key])(code[now_line].code);//指针函数执行每一条语句
        now_line++;//执行下一条语句
    }
    return 0;//执行完毕，返回
}