#pragma once
#include "basic.h"

/*输入代码定义：行数、行内容*/
typedef struct Code
{
    int num;
    string code;
} CODE;

int now_line;  /*当前行数*/
int code_size; /*所有行数*/

CODE code[CODE_SIZE]; /*全部代码*/

void loadCode(string);