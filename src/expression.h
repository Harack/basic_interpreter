#pragma once
#include "basic.h"

/*变量别名：操作数*/
typedef VAR OPERAND;

/*枚举所有操作符*/
typedef enum Operator_type
{
    //
    oper_left,
    oper_right,
    oper_add,
    oper_sub,
    oper_mult,
    oper_div,
    oper_mod,
    oper_positive,
    oper_negative,

    //
    oper_less,
    oper_greater,
    oper_equal,
    oper_ne,
    oper_le,
    oper_ge,

    //
    oper_and,
    oper_or,
    oper_not,

    oper_assign,
    oper_bottom
} opertype;

/*枚举所有操作符号的方向：左结合、右结合*/
typedef enum Ass_type
{
    left2right,
    right2left
} asstype;

/*操作数定义：操作目数；栈中优先级、栈外优先级、操作方向、操作名称*/
typedef struct Operator
{
    int num;
    int icp;
    int isp;
    asstype ass;
    opertype oper;
} OPERATOR;

/*Token类型枚举：未定义、操作数、操作符*/
typedef enum Token_type
{
    token_unknow,
    token_operand,
    token_operator
} tokentype;

/*Token定义：类型、值（操作数、操作类型）*/
typedef struct Token
{
    tokentype type;
    union
    {
        OPERAND var;
        OPERATOR oper;
    };
} TOKEN;

/*Token序列定义：栈*/
typedef struct Token_List
{
    TOKEN token;
    struct Token_List *next;
} TOKEN_LIST, *PLIST;

VAR eval(const char *);
TOKEN get_token();