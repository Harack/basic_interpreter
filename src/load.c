#include "load.h"
#include "errorhandle.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

/*载入代码*/
void loadCode(string filename)
{
    FILE *fp = fopen(filename, "r");//文件操作：读
    if (fp == NULL)
    {
        errorhandle(-1, open_file_error);
        exit(-1);
    }
    while (fscanf(fp, "%d", &code[now_line].num) != EOF)//读入每一行
    {
        if ((now_line > 0 && code[now_line].num <= code[now_line - 1].num) || code[now_line].num < 1 || code[now_line].num > 9999)
        {
            errorhandle(-1, line_num_error);
            exit(-1);
        }
        fgets(code[now_line].code, sizeof(code[now_line].code), fp);
        int st = 0, ed = (int)strlen(code[now_line].code) - 1;
        for (; isspace(code[now_line].code[st]); st++)//过滤中间多余空格
            ;
        for (; ed >= 0 && isspace(code[now_line].code[ed]); ed--)//过滤多余前导后导空格
            ;
        if (ed >= 0)
        {
            memmove(code[now_line].code, code[now_line].code + st, ed + 1);
            code[now_line].code[ed + 1] = '\0';
        }
        else
        {
            code[now_line].code[0] = '\0';
        }
        now_line++;
        if (now_line > CODE_SIZE)//假如超出行数限制，报错
        {
            errorhandle(-1, code_too_long);
            exit(-1);
        }
    }
    code_size = now_line;
    now_line = 0;
    fclose(fp);//关闭文件
    free(fp);//释放内存
}