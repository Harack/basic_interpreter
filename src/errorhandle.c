#include "errorhandle.h"
#include <stdio.h>
#include <stdlib.h>

/*根据不同类型输出错误*/
const char *errors[] = {
    "MEMORY ALLOCATION FAILED",
    "EXPRESSION STACK POP FAILED, PLEASE CHECK THE EXPRESSION",
    "EXPRESSION STACK POP FAILED, PLEASE CHECK THE EXPRESSION",
    "REDUNDANT OPERATOR",
    "UNDEFINED TOKEN TYPE",
    "VAR NOT ASSIGNED",
    "SCANF FAILED",
    "UNDEFINED OPERATOR",
    "UNDEFINED EXPRESSION",
    "OPEN TARGET FILE FAILED",
    "ILLEGAL LINE NUMBER",
    "THE TARGET FILE IS TOO LARGE",
    "PARSING CODE FAILED",
    "ILLEGAL VAR NAME",
    "FOR STACK SIZE OVER LIMIT",
    "IF STACK SIZE OVER LIMIT",
    "CON'T FIND THE PAIRED NEXT",
    "CON'T FIND THE PAIRED FOR",
    "CON'T FIND THE PAIRED END IF",
    "CON'T FIND THE PAIRED IF",
    "CON'T FIND THE TARGET LINE",
    "ILLEGAL TARGET LINE",
    "WHILE STACK SIZE OVER LIMIT",
    "CON'T FIND THE PAIRED WEND",
    "CON'T FIND THE PAIRED WHILE",
    "EXPRESSION IS EMPTY",
    "UNDEFINED KEYWORD",
};

/*输出错误所在的行和错误并结束程序*/
void errorhandle(int line, int type)
{
    if (line != -1)
    {
        printf("ERROR %d IN LINE %d: ", type, line);
    }
    printf("%s\n", errors[type]);
    char ch = getchar();
    ch = getchar();
    exit(-1);
}
