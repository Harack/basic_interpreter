#pragma once
#include "basic.h"

/*所有关键字枚举：*/
typedef enum KEYWORDS
{
    key_input,
    key_print,
    key_for,
    key_next,
    key_while,
    key_wend,
    key_if,
    key_else,
    key_endif,
    key_goto,
    key_let
} keywords;

/*FOR 循环栈定义：循环变量、开始位置、初始值、变化值*/
typedef struct FOR_STACK
{
    int start_var;
    int line;
    double target;
    double step;
} stack_for;

stack_for for_stack[MEMORY_SIZE]; /*FOR循环层数*/

/*WHILE循环栈定义：开始行、是否结束标识符*/
typedef struct WHILE_STACK
{
    int line;
    int judge;
} stack_while;

stack_while while_stack[MEMORY_SIZE]; /*WHILE循环层数*/

int if_stack[MEMORY_SIZE]; /*IF层数*/

/*当前执行跳转语句（IF、FOR、WHILE）的数量*/
typedef struct FOR_WHILE_IF_CHECK
{
    int if_count;
    int for_count;
    int while_count;
} goto_check;

goto_check all_code[CODE_SIZE]; /*每行执行跳转数量*/

void init_goto();

void exec_input(const char *);
void exec_print(const char *);
void exec_for(const char *);
void exec_next(const char *);
void exec_while(const char *);
void exec_wend(const char *);
void exec_if(const char *);
void exec_else(const char *);
void exec_endif(const char *);
void exec_goto(const char *);
void exec_assign(const char *);

int exec();