# Basic_Interpreter  
  
## 基于C语言实现的简单的Basic语言的子集的解释器  
### 武汉大学网络安全学院2021年第二学期HXY的C程序设计大作业
  
## 支持以下操作：  
    1. 输入/输出：  
        INPUT VAR  
            从键盘上获得一个数字或者字符串，并将这个数字或字符串存储在变量 VAR 中。  
        PRINT exp or PRINT "str"  
            在屏幕上输出表达式的值或者字符串。  
    2. 表达式运算：  
        支持以下运算：  
            算数：+ - * / %(MOD) +(正) -(负) ((左括号) )(右括号)  
            逻辑：AND OR NOT  
            比较：< > = != <= >=  
    3. 内存管理：  
        LET VAR = exp or LET VAR = "str"  
            将表达式的值或字符串赋值给变量 VAR 中。  
            由于技术原因， VAR 只支持 A-Z 26个字母命名。  
    4. 条件跳转：  
        WHILE  
            ...  
        WEND  
            计算表达式 exp 的值，若表达式的值非零，则执行循环，若表达式的值为零，则跳到wEND以下的语句执行。  

        FOR VAR = exp1 TO exp2 (STEP exp3)  
            ...  
        NEXT  
            将表达式 exp1 的值赋值给变量 VAR，之后执行下一条语句，直至执行到 ENDFOR 语句
            遇到 ENDFOR 语句后变量 VAR 的值加上步长，若此时变量的值小于表达式 exp2 的值，则跳回循环开始处执行循环内容，否则执行下面的语句。
            STEP exp3 部分是步长，可以省略，默认步长为1   

        IF  
            ...  
        ELSE  
            ...  
        END IF  
            计算表达式 exp 的值，若表达式的值非零，则执行IF至ELSE部分的语句，否则执行ELSE至END IF部分的语句。若没有ELSE部分则不执行。  
  
## 使用：  
    只能执行小于10000行代码，每行只能写一条语句。  
    设目标文件为target.bas  
        Windows下直接将文件用解释器打开，或在cmd中运行 Interpreter.exe target.bas
        Linux下在终端运行 ./interpreter target.bas

## 示例：
```
# code block
0001 PRINT "TEST"
0002 PRINT "PLEASE INPUT A NUMBER BETWEEN 1 AND 20"
0010 INPUT N
0011 WHILE N < 1 OR N > 20
0012	PRINT "PLEASE INPUT AGAIN"
0013 	INPUT N
0014 WEND
0020 FOR I = 1 TO N
0030 	LET L = "*"
0040 	FOR J = 1 TO N - I
0050 		LET L = " " + L
0060	NEXT
0070 	FOR J = 2 TO 2 * I - 1 STEP = 2
0080 		LET L = L + "**"
0090 	NEXT
0100 	PRINT L
0110 NEXT
0120 LET I = N-1
0130 LET L = ""
0140 FOR J = 1 TO N - I
0150 	LET L = L + " "
0160 NEXT
0170 FOR J = 1 TO ((2 * I) - 1)
0180 	LET L = L + "*"
0190 NEXT
0200 PRINT L
0210 LET I = I -1
0220 IF I > 0 THEN
0230 	GOTO 130
0240 ELSE
0250 PRINT "TSET END"
0260 END IF
```
## 结果：
Windows下:  
![example](example/Windows/example.png)  
Linux下:  
![example](example/Linux/example.png)  

## 其他：
思路与测试样例参考 [《用C语言写解释器》](http://blog.csdn.net/redraiment/article/details/4693952)
    